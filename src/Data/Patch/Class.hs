{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TypeFamilies               #-}
module Data.Patch.Class
  ( -- Classes
    Patchable
  , Applicable(..)
  , Composable(..)
  , Transformable(..)
  , Diff(..)

  -- Type families
  , Patched
  , ConflictResolution

  -- Functions
  , transform

  -- Basic conflict resolution types and strategies
  , SimpleConflictResolution
  , SimpleConflictResolution'
  , ours
  , theirs

  , Replaceable(..)
  , _Del
  , _New

  -- Replace (TODO move to its own module)
  , Replace
  , ConflictResolutionReplace

  -- PairPatch (TODO move to its own module)
  , PairPatch(..)
  , fstPatch
  , sndPatch

  -- PairPatch (TODO move to its own module)
  , EitherPatch(..)
  , leftPatch
  , rightPatch

  -- MaybePatch (TODO move to its own module)
  , MaybePatch(Mod)
  , unMod

  -- Re-exports
  , Action(..)
  , Group(..)
  , Monoid(..)
  )
  where

import Data.Aeson
import Data.Bifunctor
import Data.Bifoldable
import Data.Bitraversable
import Data.Group
import Data.Semigroup hiding ((<>), diff)
import Data.Monoid.Action
import Data.Patch.Class.Utils
import Data.Tuple
import Data.Validity
import GHC.Generics (Generic)
import Lens.Micro

type family Patched a

class Applicable p a where
  -- When @isValid (applicable p a)@ then @act p a@ is well defined.
  applicable :: p -> a -> Validation

class Monoid a => Composable a where
  -- When @isValid (composable x y)@ then @x <> y@ is well defined.
  composable :: a -> a -> Validation

type SimpleConflictResolution' a = a -> a -> a
type SimpleConflictResolution p = SimpleConflictResolution' (Patched p)

type family ConflictResolution p

class Transformable p where
  -- | Given two diverging patches @p@ and @q@, @transformWith m p q@ returns
  --   a pair of updated patches @(p',q')@ such that @p' <> q@ and
  --   @q' <> p@ are equivalent patches that incorporate the changes
  --   of /both/ @p@ and @q@, up to merge conflicts, which are handled by
  --   the provided function @m@.
  --
  --   (Recall that the patch @p <> q@ applies @q@ then @p@.
  --    @act (p <> q) == act p . act q@)
  --
  --   This is the standard *transform* function of Operational Transformation
  --   patch resolution techniques, and can be thought of as the pushout
  --   of two diverging patches within the patch groupoid.
  --
  --   prop> forAll (divergingPatchesFrom d) $ \(p,q) -> let (p', q') = transformWith ours p q in act (p' <> q) d == act (q' <> p) d
  --   prop> forAll (divergingPatchesFrom d) $ \(p,q) -> let (p', q') = transformWith ours p q in applicable p' (act q d) && applicable q' (act p d)
  --   prop> forAll (divergingPatchesFrom d) $ \(p,q) -> let (p', q') = transformWith ours p q in composable p' q && composable q' p
  --
  --   This function is commutative iff @m@ is commutative.
  --
  --   prop> forAll (divergingPatchesFrom d) $ \(p,q) -> let (p', q') = transformWith (*) p q; (q'', p'') = transformWith (*) q p in p' == p'' && q' == q''
  --
  --   prop> forAll (patchesFrom d) $ \ p -> transformWith (*) mempty p == (mempty, p)
  --   prop> forAll (patchesFrom d) $ \ p -> transformWith (*) p mempty == (p, mempty)
  transformWith :: ConflictResolution p -> p -> p -> (p, p)
  transformWith m p q = (transformFst m p q, transformSnd m p q)

  transformFst :: ConflictResolution p -> p -> p -> p
  transformFst m p q = fst $ transformWith m p q

  transformSnd :: ConflictResolution p -> p -> p -> p
  transformSnd m p q = snd $ transformWith m p q
  -- If we could generically /flip/ on @ConflictResolution@ then we could
  -- implement @transformSnd@ using @transformFst@.
  -- transformSnd = flip . transformFst . flipConflictResolution

  -- If @isValid (transformable p q)@ then @transformWith m p q@ is well defined.
  transformable :: p -> p -> Validation

  -- @conflicts p q@ sums up the number of conflicts.
  -- Namely this is the number of times that @m@ would be used to
  -- compute @transformWith m p q@.
  -- One can use @getSum@ to retreive the sum.
  conflicts :: p -> p -> Sum Int

  {-# MINIMAL (transformWith | (transformFst, transformSnd)),
              transformable, conflicts #-}

-- | TODO Doc, see 'Applicable', 'Action', 'Composable', and 'Group'.
class (Eq p, Group p, Composable p, Validity p,
       Action p (Patched p),
       Applicable p (Patched p))
   => Patchable p
-- `Diff (Patched p) p` does not fit well here yet.

-- | A convenience version of 'transformWith' which resolves conflicts using 'mappend'.
transform :: ( Transformable p
             , Monoid (Patched p)
             , ConflictResolution p ~ SimpleConflictResolution p
             )
          => p -> p -> (p, p)
transform = transformWith (<>)

--class Patched p ~ a => Diffable a p where
--  diffable :: a -> a -> Validation

class Diff a p where
  -- | Compute the difference between two values.
  --
  -- prop> act (diff d e) d == e
  --
  -- prop> diff d d == mempty
  --
  -- prop> act (diff d e) d == act (invert (diff e d)) d
  --
  -- prop> act (diff b c <> diff a b) a == act (diff a c) a
  --
  -- prop> applicable (diff a b) a
  --
  diff :: a -> a -> p

class Replaceable p a | p -> a where
  -- TODO doc
  replace :: a -> a -> p

  -- | A traversal for the old element to be replaced.
  old :: Traversal' p a

  -- | A traversal for the new value to be replaced.
  new :: Traversal' p a

_New :: Replaceable p (Maybe a) => a -> p
_New = replace Nothing . Just

_Del :: Replaceable p (Maybe a) => a -> p
_Del a = replace (Just a) Nothing

--------------
-- Replace ---
--------------

data Replace a
  = Keep
  | Replace { _old, _new :: a }
  -- ^ Replace is only valid if the two values are different.
  deriving (Eq, Ord, Show, Generic, Read)

instance ToJSON a => ToJSON (Replace a) where
  toJSON     = genericToJSON     $ jsonOptions "_"
  toEncoding = genericToEncoding $ jsonOptions "_"

instance FromJSON a => FromJSON (Replace a) where
  parseJSON = genericParseJSON $ jsonOptions "_"

{-
instance ToSchema a => ToSchema (Replace a) where
  declareNamedSchema _ = do
    aSchema <- declareSchemaRef (Proxy :: Proxy a)
    return $ NamedSchema (Just "Replace") $ mempty
      & type_ .~ SwaggerObject
      & properties .~
          [ ("old", aSchema)
          , ("new", aSchema)
          ]
      & required .~ [ "old", "new" ]
-}

instance Eq a => Replaceable (Replace a) a where
  replace x y | x == y    = Keep
              | otherwise = Replace x y

  -- The traversal is empty for @Keep@.
  old _ Keep          = pure Keep
  old f (Replace o n) = (`replace` n) <$> f o

  -- The traversal is empty for @Keep@.
  new _ Keep          = pure Keep
  new f (Replace o n) = replace o <$> f n

type ConflictResolutionReplace a = SimpleConflictResolution' a
type instance ConflictResolution (Replace a) = ConflictResolutionReplace a

instance Semigroup (Replace a) where
  Keep <> r = r
  r <> Keep = r
  Replace _ n <> Replace o _ = Replace o n

instance Monoid (Replace a) where
  mempty = Keep

instance Group (Replace a) where
  invert Keep = Keep
  invert (Replace o n) = Replace n o

instance Eq a => Composable (Replace a) where
  composable Keep _ = mempty
  composable _ Keep = mempty
  composable (Replace o _) (Replace _ n) =
    check (n == o) "Composing two @Replace@ patches with a different inner value"

{-
Another tempting option is to have the patch only replaces `old` by `new` when
the source is equal to `old` and leaves it unchanged otherwise.

However this semantics is:
* not compatible with the Transformable instance!
* not stable under composition
instance Eq a => Action (Replace a) a where
  act Keep a = a
  act (Replace o n) source
    | source == o = n
    | otherwise   = source
-}

-- {- PRODUCTION instance
{- The patch is assumed to only be used on a source which is known
   to be equal to `old`.
-}
instance Action (Replace a) a where
  act Keep a = a
  act (Replace _ n) _ = n
-- -}

{- DEBUGGING instance
instance (Show a, Eq a) => Action (Replace a) a where
  act Keep a = a
  act p@(Replace expected n) found
    | expected == found = n
    | otherwise         =
        error $ unlines
          [ "Data.Patch.Class.act: invalid patch"
          , "Expected: " <> show expected
          , "Found:    " <> show found
          , "Patch:    " <> show p
          ]
-}

instance Eq a => Applicable (Replace a) a where
  applicable Keep          _ = mempty
  applicable (Replace o _) a = check (o == a) "Applying a value different than the *old* value of a Replace patch"

type instance Patched (Replace a) = a

instance Eq a => Validity (Replace a) where
  validate Keep = mempty
  validate (Replace o n) = check (o /= n) "_old and _new should be different"

instance Eq a => Transformable (Replace a) where
  transformable Keep _ = mempty
  transformable _ Keep = mempty
  transformable (Replace ox _) (Replace oy _) =
    check (ox == oy) "Transforming two @Replace@ patches should have a common origin"

  conflicts Keep _ = Sum 0
  conflicts _ Keep = Sum 0
  conflicts (Replace _ nx) (Replace _ ny)
    | nx == ny  = Sum 0
    | otherwise = Sum 1

  transformWith _ Keep p = (Keep, p)
  transformWith _ p Keep = (p, Keep)
  transformWith m (Replace _ nx) (Replace _ ny)
    | nx == ny  = (Keep, Keep)
    | otherwise = (replace ny n, replace nx n)
        where n = m nx ny

instance Eq a => Diff a (Replace a) where
  diff = replace

instance Eq a => Patchable (Replace a)

----------
-- (,) ---
----------

instance (Composable a, Composable b) => Composable (a, b) where
  composable (px, py) (qx, qy) =
    decorate "(,): Composing the two first patches"  (composable px qx) <>
    decorate "(,): Composing the two second patches" (composable py qy)

type instance ConflictResolution (p, q) =
  (ConflictResolution p, ConflictResolution q)

{-ORPHAN
instance ( Action p a
         , Action q b
         , Bifunctor f
         ) => Action (p, q) (f a b) where
  act (p, q) = bimap (act p) (act q)
-}

actPair :: ( Action p a
           , Action q b
           , Bifunctor f
           ) => (p, q) -> f a b -> f a b
actPair (p, q) = bimap (act p) (act q)

instance ( Applicable p a
         , Applicable q b
         , Bifoldable f
         ) => Applicable (p, q) (f a b) where
  applicable (p, q) =
    bifoldMap
      (decorate "(,): Applying the first patch to the first value" . applicable p)
      (decorate "(,): Applying the second patch to the second value" . applicable q)

instance ( Transformable p
         , Transformable q
         ) => Transformable (p, q) where
  transformable (px, py) (qx, qy) =
    decorate "(,): Transforming the firsts patches" (transformable px qx) <>
    decorate "(,): Transforming the seconds patches" (transformable py qy)

  conflicts (px, py) (qx, qy) = conflicts px qx <> conflicts py qy

  transformWith (mx, my) (px, py) (qx, qy) = ((px', py'), (qx', qy'))
    where
      (px', qx') = transformWith mx px qx
      (py', qy') = transformWith my py qy

  transformFst (mx, my) (px, py) (qx, qy) = (px', py')
    where
      px' = transformFst mx px qx
      py' = transformFst my py qy

  transformSnd (mx, my) (px, py) (qx, qy) = (qx', qy')
    where
      qx' = transformSnd mx px qx
      qy' = transformSnd my py qy

instance (Diff a p, Diff b q) => Diff (a, b) (p, q) where
  diff (a, b) (a', b') = (diff a a', diff b b')

-----------
-- Pair ---
-----------

newtype PairPatch p q = PairPatch { _pairPatch :: (p, q) }
  deriving (Eq, Ord, Show, Generic, Read,
            Bifunctor, Bifoldable, Semigroup, Monoid, Group)

instance Bitraversable PairPatch where
  bitraverse f g (PairPatch p) = PairPatch <$> bitraverse f g p

fstPatch :: Lens (PairPatch p q) (PairPatch p' q) p p'
fstPatch f (PairPatch (p, q)) = PairPatch . flip (,) q <$> f p

sndPatch :: Lens (PairPatch p q) (PairPatch p q') q q'
sndPatch f (PairPatch (p, q)) = PairPatch . (,) p <$> f q

instance (Validity p, Validity q) => Validity (PairPatch p q) where
  validate (PairPatch p) = annotate p "PairPatch"

instance (Composable a, Composable b) => Composable (PairPatch a b) where
  composable (PairPatch p) (PairPatch q) = decorate "PairPatch" (composable p q)

instance ( Transformable p
         , Transformable q
         ) => Transformable (PairPatch p q) where
  transformable (PairPatch p) (PairPatch q) = decorate "PairPatch" (transformable p q)
  conflicts (PairPatch p) (PairPatch q) = conflicts p q
  transformWith m (PairPatch p) (PairPatch q) = bimap PairPatch PairPatch (transformWith m p q)
  transformFst m (PairPatch p) (PairPatch q) = PairPatch (transformFst m p q)
  transformSnd m (PairPatch p) (PairPatch q) = PairPatch (transformSnd m p q)

instance ( Action p a
         , Action q b
      -- , a ~ Patched p WHY?
      -- , b ~ Patched q WHY?
         ) => Action (PairPatch p q) (a, b) where
  act = actPair . _pairPatch

instance ( Applicable p a
         , Applicable q b
      -- , a ~ Patched p WHY?
      -- , b ~ Patched q WHY?
         ) => Applicable (PairPatch p q) (a, b) where
  applicable = applicable . _pairPatch

deriving instance (Diff a p, Diff b q) => Diff (a, b) (PairPatch p q)

type instance Patched (PairPatch p q) = (Patched p, Patched q)

type instance ConflictResolution (PairPatch p q) =
  (ConflictResolution p, ConflictResolution q)

instance (Patchable p, Patchable q) => Patchable (PairPatch p q)

{-
instance Bifunctor PairPatch where
  bimap f g (p :*: q) = f p :*: g q
  first f (p :*: q) = f p :*: q
  second g (p :*: q) = p :*: g q

fstPatch :: Lens (PairPatch p q) (PairPatch p' q) p p'
fstPatch f (p :*: q) = (:*: q) <$> f p

sndPatch :: Lens (PairPatch p q) (PairPatch p q') q q'
sndPatch f (p :*: q) = (p :*:) <$> f q

instance (Semigroup p, Semigroup q) => Semigroup (PairPatch p q) where
  (px :*: py) <> (qx :*: qy) = (px <> qx) :*: (py <> qy)

instance (Monoid p, Monoid q) => Monoid (PairPatch p q) where
  mempty = mempty :*: mempty

instance (Group p, Group q) => Group (PairPatch p q) where
  invert (p :*: q) = invert p :*: invert q

instance (Composable a, Composable b) => Composable (PairPatch a b) where
  composable (px :*: py) (qx :*: qy) =
    decorate "PairPatch: Composing the two first patches"  (composable px qx) <>
    decorate "PairPatch: Composing the two second patches" (composable py qy)

instance ( Action p a
         , Action q b
         , a ~ Patched p
         , b ~ Patched q
         , Bifunctor f
         ) => Action (PairPatch p q) (f a b) where
  act (p :*: q) (x, y) = (act p x, act q y)

instance ( Applicable p a
         , Applicable q b
         , a ~ Patched p
         , b ~ Patched q
         , Bifoldable f
         ) => Applicable (PairPatch p q) (f a b) where
  applicable (p :*: q) =
    bifoldMap
      (decorate "PairPatch: Applying the first patch to the first value" . applicable p)
      (decorate "PairPatch: Applying the second patch to the second value" . applicable q)

instance (Validity p, Validity q) => Validity (PairPatch p q) where
  validate (p :*: q) =
    annotate p "PairPatch: Validating the first patch of the PairPatch" <>
    annotate q "PairPatch: Validating the second patch of the PairPatch"

instance ( Transformable p
         , Transformable q
         ) => Transformable (PairPatch p q) where
  transformable (px :*: py) (qx :*: qy) =
    decorate "PairPatch: Transforming the firsts patches" (transformable px qx) <>
    decorate "PairPatch: Transforming the seconds patches" (transformable py qy)

  conflicts (px :*: py) (qx :*: qy) = conflicts px qx <> conflicts py qy

  transformWith (mx, my) (px :*: py) (qx :*: qy) = ((px' :*: py'), (qx' :*: qy'))
    where
      (px', qx') = transformWith mx px qx
      (py', qy') = transformWith my py qy

  transformFst (mx, my) (px :*: py) (qx :*: qy) = px' :*: py'
    where
      px' = transformFst mx px qx
      py' = transformFst my py qy

  transformSnd (mx, my) (px :*: py) (qx :*: qy) = qx' :*: qy'
    where
      qx' = transformSnd mx px qx
      qy' = transformSnd my py qy

instance (Diff a p, Diff b q) => Diff (a, b) (PairPatch p q) where
  diff (a, b) (a', b') = diff a a' :*: diff b b'
-}

-------------
-- Either ---
-------------

newtype EitherPatch p q = EitherPatch { _eitherPatch :: (p, q) }
  deriving (Eq, Ord, Show, Generic, Read,
            Bifunctor, Bifoldable, Semigroup, Monoid, Group)

leftPatch :: Lens (EitherPatch p q) (EitherPatch p' q) p p'
leftPatch f (EitherPatch (p, q)) = EitherPatch . flip (,) q <$> f p

rightPatch :: Lens (EitherPatch p q) (EitherPatch p q') q q'
rightPatch f (EitherPatch (p, q)) = EitherPatch . (,) p <$> f q

type instance Patched (EitherPatch p q) = Either (Patched p) (Patched q)

type instance ConflictResolution (EitherPatch p q) =
  (ConflictResolution p, ConflictResolution q)

instance (Composable a, Composable b) => Composable (EitherPatch a b) where
  composable (EitherPatch p) (EitherPatch q) = decorate "EitherPatch" (composable p q)

instance (Validity p, Validity q) => Validity (EitherPatch p q) where
  validate (EitherPatch p) = annotate p "EitherPatch"

instance ( Transformable p
         , Transformable q
         ) => Transformable (EitherPatch p q) where
  transformable (EitherPatch p) (EitherPatch q) = decorate "EitherPatch" (transformable p q)
  conflicts (EitherPatch p) (EitherPatch q) = conflicts p q
  transformWith m (EitherPatch p) (EitherPatch q) = bimap EitherPatch EitherPatch (transformWith m p q)
  transformFst m (EitherPatch p) (EitherPatch q) = EitherPatch (transformFst m p q)
  transformSnd m (EitherPatch p) (EitherPatch q) = EitherPatch (transformSnd m p q)

instance ( Action p a
         , Action q b
      -- , a ~ Patched p WHY?
      -- , b ~ Patched q WHY?
         ) => Action (EitherPatch p q) (Either a b) where
  act = actPair . _eitherPatch

instance ( Applicable p a
         , Applicable q b
      -- , a ~ Patched p WHY?
      -- , b ~ Patched q WHY?
         ) => Applicable (EitherPatch p q) (Either a b) where
  applicable = applicable . _eitherPatch

{-

_EitherPatch :: Iso' (EitherPatch p q) (p, q)
_EitherPatch = iso _eitherPatch EitherPatch

instance Bifunctor EitherPatch where
  bimap f g (p :+: q) = f p :+: g q
  first f (p :+: q) = f p :+: q
  second g (p :+: q) = p :+: g q

leftPatch :: Lens (EitherPatch p q) (EitherPatch p' q) p p'
leftPatch f (p :+: q) = (:+: q) <$> f p

rightPatch :: Lens (EitherPatch p q) (EitherPatch p q') q q'
rightPatch f (p :+: q) = (p :+:) <$> f q

instance (Semigroup p, Semigroup q) => Semigroup (EitherPatch p q) where
  px :+: py <> qx :+: qy = (px <> qx) :+: (py <> qy)

instance (Monoid p, Monoid q) => Monoid (EitherPatch p q) where
  mempty = mempty :+: mempty

instance (Group p, Group q) => Group (EitherPatch p q) where
  invert (p :+: q) = invert p :+: invert q

instance (Composable a, Composable b) => Composable (EitherPatch a b) where
  composable (px :+: py) (qx :+: qy) =
    decorate "EitherPatch: Composing the two first patches"  (composable px qx) <>
    decorate "EitherPatch: Composing the two second patches" (composable py qy)

instance (Validity p, Validity q) => Validity (EitherPatch p q) where
  validate (p :+: q) =
    annotate p "EitherPatch: Validating the first patch" <>
    annotate q "EitherPatch: Validating the second patch"

instance ( Action p a
         , Action q b
         , a ~ Patched p
         , b ~ Patched q
         ) => Action (EitherPatch p q) (Either a b) where
  act (p :+: _) (Left  x) = Left  (act p x)
  act (_ :+: p) (Right x) = Right (act p x)

instance ( Applicable p a
         , Applicable q b
         , a ~ Patched p
         , b ~ Patched q
         ) => Applicable (EitherPatch p q) (Either a b) where
  applicable (p :+: _) (Left  x)
    = decorate "EitherPatch: Applying the first patch"  $ applicable p x
  applicable (_ :+: p) (Right x)
    = decorate "EitherPatch: Applying the second patch" $ applicable p x

instance ( Transformable p
         , Transformable q
         ) => Transformable (EitherPatch p q) where
  transformable (px :+: py) (qx :+: qy) =
    decorate "EitherPatch: Transforming the firsts patches" (transformable px qx) <>
    decorate "EitherPatch: Transforming the seconds patches" (transformable py qy)

  conflicts (px :+: py) (qx :+: qy) = conflicts px qx <> conflicts py qy

  transformWith (mx, my) (px :+: py) (qx :+: qy) = ((px' :+: py'), (qx' :+: qy'))
    where
      (px', qx') = transformWith mx px qx
      (py', qy') = transformWith my py qy

  transformFst (mx, my) (px :+: py) (qx :+: qy) = px' :+: py'
    where
      px' = transformFst mx px qx
      py' = transformFst my py qy

  transformSnd (mx, my) (px :+: py) (qx :+: qy) = qx' :+: qy'
    where
      qx' = transformSnd mx px qx
      qy' = transformSnd my py qy
-}

instance (Diff (Maybe a) p, Diff (Maybe b) q, Monoid p, Monoid q)
      => Diff (Either a b) (EitherPatch p q) where
  diff (Left  a) (Left  a') = EitherPatch ( diff (Just a) (Just a')
                                          , mempty
                                          )
  diff (Right b) (Right b') = EitherPatch ( mempty
                                          , diff (Just b) (Just b')
                                          )
  diff (Left  a) (Right b') = EitherPatch ( diff (Just a) Nothing
                                          , diff Nothing  (Just b')
                                          )
  diff (Right b) (Left  a') = EitherPatch ( diff Nothing  (Just a')
                                          , diff (Just b) Nothing
                                          )

instance (Patchable p, Patchable q) => Patchable (EitherPatch p q)

------------
-- Maybe ---
------------

-- |
-- Patch witch acts on a Maybe value.
--
-- TODO?:
--
--   * transformWith m (Mod p) (mkDel x) = (mkDel (act p x), mempty)
--
--   * transformWith m (Mod p) (mkNew x) = impossible
--
--   * transformWith m (mkNew x) (mkNew y) = (replace y z, replace x z)
--       where z = m x y
data MaybePatch a p
  = Rpl { _mp_old :: Maybe a, _mp_new :: Maybe a }
  | Mod p
  deriving (Eq, Ord, Show, Generic, Read)
--            Bifunctor, Bifoldable, Semigroup, Monoid, Group)

instance (Monoid p, Eq a) => Replaceable (MaybePatch a p) (Maybe a) where
  replace o n | o == n    = Mod mempty
              | otherwise = Rpl o n
  old f (Rpl o n) = (`replace` n) <$> f o
  old _ p         = pure p
  new f (Rpl o n) = replace o <$> f n
  new _ p         = pure p

unMod :: MaybePatch a p -> Maybe p
unMod Rpl{}   = Nothing
unMod (Mod p) = Just p

type instance Patched (MaybePatch a p) = Maybe a

type instance ConflictResolution (MaybePatch a p) =
  (ConflictResolutionReplace (Maybe a),
   ConflictResolution p,
   (Bool {- prefer left Rpl over Mod-}, Bool {- prefer right Rpl over Mod-})
   -- ^ TODO make more explicit
  )

instance (Eq a, Action p a, Group p) => Semigroup (MaybePatch a p) where
  Rpl _ n <> Rpl o _ = replace o n
  Mod p   <> Mod q   = Mod (p <> q) -- p & q != mempty
  Mod p   <> Rpl o n = replace o (act p <$> n)
  Rpl o n <> Mod p   = replace (act (invert p) <$> o) n

instance (Eq a, Action p a, Group p) => Monoid (MaybePatch a p) where
  mempty = Mod mempty

instance (Eq a, Action p a, Group p) => Group (MaybePatch a p) where
  invert (Rpl o n) = Rpl n o
  invert (Mod p)   = Mod (invert p) -- p != mempty => invert p != mempty

instance (Eq a, Group p, Action p a, Applicable p a, Composable p)
      => Composable (MaybePatch a p) where
  composable (Rpl o _) (Rpl _ n) =
    check (n == o) "Composing two @Rpl@ patches with a different inner value"
  composable (Mod p)   (Mod q)   = decorate "composable/MaybePatch/Mod" (composable p q)
  composable (Mod p)   (Rpl _ n) = decorate "composable/MaybePatch/Mod&Rpl" (foldMap (applicable p) n)
  composable (Rpl o _) (Mod p)   = decorate "composable/MaybePatch/Rpl&Mod" (foldMap (applicable (invert p)) o)

instance (Eq a, Action p a) => Action (MaybePatch a p) (Maybe a) where
  act (Rpl _ n) = const n
  act (Mod p)   = fmap (act p)

instance (Eq a, Applicable p a) => Applicable (MaybePatch a p) (Maybe a) where
  applicable (Rpl o _) a = check (a == o) "MaybePatch/Rpl: Applying a value different than the *old* value of a Rpl patch"
  applicable (Mod p)   a = decorate "applicable/MaybePatch/Mod" $ foldMap (applicable p) a

instance (Eq a, Monoid p, Eq p, Validity p) => Validity (MaybePatch a p) where
  validate (Rpl o n) = check (o /= n) "MaybePatch: old and new should be different"
  validate (Mod p)   = check (p /= mempty) "MaybePatch/Mod should not be mempty"
                    <> validate p

instance (Eq a, Eq p, Group p, Action p a, Applicable p a, Transformable p)
      => Transformable (MaybePatch a p) where
  transformWith (m, _, _) (Rpl _ nx) (Rpl _ ny)
    | nx == ny  = (mempty, mempty)
    | otherwise = (replace ny n, replace nx n)
        where n = m nx ny
  transformWith (_, m, _) (Mod p) (Mod q) = bimap Mod Mod (transformWith m p q)

  transformWith (_, _, (True, _)) (Rpl (Just o) mn) (Mod q)
  --                    ^ We give priority to the Rpl over the Mod.
    = ( replace (Just (act q o)) mn
      , mempty
      )
      {-
      P  = Rpl (Just o) mn
      Q  = Mod q
      P' = replace (Just (act q o)) mn
      Q' = mempty

      P' <> Q == Q' <> P

      if replace == Rpl

      Rpl (Just (act q o)) mn <> Mod q
      ={def (<>)}=
      Rpl (act (invert q) <$> (Just (act q o))) mn
      ={def (<$>)}=
      Rpl (Just (act (invert q) (act q o))) mn
      ={act-invert}=
      Rpl (Just o) mn
      ={mempty-left-<>}=
      mempty <> Rpl (Just o) mn
      -}
  transformWith (_, _, (False, _)) (Rpl (Just o) (Just n)) (Mod q)
  --                    ^ We give priority to the Mod over the Rpl.
    = ( Rpl (Just (act q o)) (Just (act q n))
      , Mod q
      )
      {-
      P  = Rpl (Just o) (Just n)
      Q  = Mod q
      P' = replace (Just (act q o)) (Just (act q n))
      Q' = Mod q

      P' <> Q == Q' <> P

      if replace == Rpl

      Rpl (Just (act q o)) (Just (act q n)) <> Mod q
      ={def (<>)}=
      Rpl (act (invert q) <$> Just (act q o)) (Just (act q n))
      ={def (<$>)}=
      Rpl (Just (act (invert q) (act q o))) (Just (act q n))
      ={act-invert}=
      Rpl (Just o) (Just (act q n))
      ={def (<$>)}=
      Rpl (Just o) (act q <$> Just n)
      ={def (<>)}=
      Mod q <> Rpl (Just o) (Just n)
      -}
  transformWith (_, _, m) p@(Mod _) q@(Rpl _ _)
    = swap $ transformWith (undefined, undefined, swap m) q p

  transformWith _      _       _       = error "transformWith/MaybePatch"

  transformable (Rpl ox _) (Rpl oy _) =
    check (ox == oy) "Transforming two @Rpl@ patches should have a common origin"
  transformable (Mod p) (Mod q) = transformable p q
  transformable (Mod p) (Rpl (Just o) (Just n)) = applicable p o <> applicable p n
  transformable (Rpl (Just o) (Just n)) (Mod q) = applicable q o <> applicable q n
  transformable (Mod _) (Rpl _ _) = invalid "transformable/MaybePatch: Mod vs Rpl"
  transformable (Rpl _ _) (Mod _) = invalid "transformable/MaybePatch: Rpl vs Mod"

  conflicts (Rpl ox nx) (Rpl oy ny) = conflicts (Replace ox nx) (Replace oy ny)
  conflicts (Mod p) (Mod q) = conflicts p q
  conflicts Rpl{} Mod{} = Sum 1
  conflicts Mod{} Rpl{} = Sum 1

instance (Patched p ~ a, Eq a, Patchable p) => Patchable (MaybePatch a p)

-- | Resolve a conflict by always using the left-hand side
ours :: a -> a -> a
ours = const

-- | Resolve a conflict by always using the right-hand side
theirs :: a -> a -> a
theirs = flip const
